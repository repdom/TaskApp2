package di.edu.itla.taskapp.vista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import di.edu.itla.taskapp.R;
import di.edu.itla.taskapp.entidad.Categoria;
import di.edu.itla.taskapp.repositorio.CategoriaRepositorio;
import di.edu.itla.taskapp.repositorio.db.CategoriaRepositorioImp;

public class CategoriaListaActivity extends AppCompatActivity {

    private static final String LOG_TAG =  "CategoriaListaActivity";
    private CategoriaRepositorio categoriaRepositorio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria_lista);

        categoriaRepositorio = new CategoriaRepositorioImp(this);
        List<Categoria> categorias = categoriaRepositorio.buscar(null);
        Log.i(LOG_TAG,"Total de categorias: "+categorias.size());

        ListView catListView = (ListView)findViewById(R.id.catListView);
        catListView.setAdapter(new CategoriaListAdapter(this,categorias));


        catListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Categoria cat=(Categoria) adapterView.getItemAtPosition(i);
                Intent regCatIntent = new Intent(CategoriaListaActivity.this,CategoriaActivity.class);
                //regCatIntent.putExtra("categoria",cat);
                startActivity(regCatIntent);
            }
        });



    }
}
