package di.edu.itla.taskapp.entidad;

public class Usuario {
    private Integer id;
    private String nombre;
    private String email;
    private String contrasena;
    TipoUsuario tipoUsuario;

    public enum TipoUsuario {
        TECNICO, NORMAL
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Usuario{");
        sb.append("id=").append(id);
        sb.append(", nombre='").append(nombre).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", contrasena='").append(contrasena).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
