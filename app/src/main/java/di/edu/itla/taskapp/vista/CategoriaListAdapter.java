package di.edu.itla.taskapp.vista;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import di.edu.itla.taskapp.R;
import di.edu.itla.taskapp.entidad.Categoria;

public class CategoriaListAdapter extends BaseAdapter{

    private Context context;
    private List<Categoria> categorias;

    @Override
    public int getCount() {
        return categorias.size();
    }

    public CategoriaListAdapter(Context context, List<Categoria> categorias) {
        this.context = context;
        this.categorias = categorias;
    }

    @Override
    public Object getItem(int position) {
        return categorias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categorias.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        if (view == null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.categoria_listview_row,null,true);
        }
        TextView lbCategoriaId = view.findViewById(R.id.lbCategoriaId);
        TextView lbCategoriaNombre = view.findViewById(R.id.lbNombreCategoria);

        Categoria cat = categorias.get(position);

        lbCategoriaId.setText(cat.getId().toString());
        lbCategoriaNombre.setText(cat.getNombre().toString());
        return view;
    }
}
